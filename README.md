# Paragraphs in REST

Consider this module if you're building modular content with Paragraphs and
you want to very quickly create an API to access all of your content.

This module ensures that fields attached to each of your Paragraphs will be
recursively loaded. It also supports paragraphs nested inside other paragraphs
with as many levels deep as you'd like, so you can expose all of the content on
any given node to through an API without requiring multiple requests to
"expand" each paragraph item individually.

Specifically, this works with JSON and XML services defined by the core
RESTful Web Services (rest) module. This does not work well with JSON:API.


## Table of contents

- Requirements
- Recommended modules
- Installation
- Configuration
- Maintainers


## Requirements

Drupal core: ^10


## Recommended modules

[Paragraphs] (https://www.drupal.org/project/paragraphs)


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

Enable the following modules -
  - Paragraphs in REST
  - RESTful Web Services
  - Serialization
  - Paragraphs
  - Entity Reference Revisions


## Maintainers

- gslexie - https://www.drupal.org/u/gslexie
