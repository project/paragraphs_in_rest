<?php

namespace Drupal\paragraphs_in_rest\Normalizer;

use Drupal\entity_reference_revisions\EntityReferenceRevisionsFieldItemList;
use Drupal\serialization\Normalizer\NormalizerBase;

/**
 * {@inheritdoc}
 */
class NestedERRNormalizer extends NormalizerBase {

  /**
   * {@inheritdoc}
   */
  public function normalize($field, $format = NULL, array $context = []): array|string|int|float|bool|\ArrayObject|NULL {
    $serializer = $this->serializer;
    $normalizedItems = [];
    foreach ($field as $item) {
      $referenced_entity = $item->entity;
      $normalizedItems[] = $serializer->normalize($referenced_entity, $format, $context);
    }
    return $normalizedItems;

  }

  /**
   * {@inheritdoc}
   */
  public function getSupportedTypes(?string $format): array {
    if ($format == 'json' || $format == 'xml') {
      return [
        EntityReferenceRevisionsFieldItemList::class => TRUE,
      ];
    }
    else {
      return [];
    }
  }

}
