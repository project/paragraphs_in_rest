<?php

namespace Drupal\Tests\paragraphs_in_rest\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\paragraphs\FunctionalJavascript\ParagraphsTestBaseTrait;

/**
 * Tests that nested paragraphs are displaying properly in XML REST apis.
 *
 * @group paragraphs_in_rest
 */
class ApiTest extends BrowserTestBase {

  use ParagraphsTestBaseTrait;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['node', 'paragraphs_in_rest', 'field_ui'];

  /**
   * Theme to enable.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $type = $this->container->get('entity_type.manager')->getStorage('paragraphs_type')
      ->create([
        'id' => 'text',
        'label' => 'Text',
      ]);
    $type->save();
    $this->addFieldtoParagraphType('text', 'field_text', 'text');

    $type = $this->container->get('entity_type.manager')->getStorage('paragraphs_type')
      ->create([
        'id' => 'columns',
        'label' => 'Columns',
      ]);
    $type->save();
    $this->addParagraphsField('columns', 'field_column1', 'paragraph');
    $this->addParagraphsField('columns', 'field_column2', 'paragraph');

    $type = $this->container->get('entity_type.manager')->getStorage('node_type')
      ->create([
        'type' => 'article',
        'name' => 'Article',
      ]);
    $type->save();
    $this->addParagraphsField('article', 'field_components', 'node');

    $paragraphStorage = $this->container->get('entity_type.manager')->getStorage('paragraph');
    $paragraph1 = $paragraphStorage->create(
      ['type' => 'text', 'field_text' => 'Lorem Ipsum Column 1']
    );
    $paragraph1->save();
    $paragraph2 = $paragraphStorage->create(
      ['type' => 'text', 'field_text' => 'Lorem Ipsum Column 2']
    );
    $paragraph2->save();

    $paragraphColumn = $paragraphStorage->create([
      'type' => 'columns',
      'field_column1' => [[
        'target_id' => $paragraph1->id(),
        'target_revision_id' => $paragraph1->getRevisionId(),
      ],
      ],
      'field_column2' => [[
        'target_id' => $paragraph2->id(),
        'target_revision_id' => $paragraph2->getRevisionId(),
      ],
      ],
    ]);
    $paragraphColumn->save();

    $node = $this->drupalCreateNode([
      'type' => 'article',
      'field_components' => [[
        'target_id' => $paragraphColumn->id(),
        'target_revision_id' => $paragraphColumn->getRevisionId(),
      ],
      ],
    ]);
    $node->save();

    $restStorage = $this->container->get('entity_type.manager')->getStorage('rest_resource_config');
    $rest = $restStorage->create([
      'id' => 'entity.node',
      'plugin_id' => 'entity:node',
      'granularity' => 'resource',
      'configuration' => [
        'methods' => ['GET'],
        'formats' => ['json', 'xml'],
        'authentication' => ['cookie'],
      ],
    ]);
    $rest->save();

    $this->container->get('router.builder')->rebuild();
  }

  /**
   * Tests that the nested paragraphs load in a JSON service.
   */
  public function testJson() {
    $account = $this->drupalCreateUser([]);
    $this->drupalLogin($account);

    $body = $this->drupalGet("node/1", ['query' => ['_format' => 'json']]);
    $this->assertSession()->statusCodeEquals(200);

    $data = json_decode($body, TRUE);
    $col1_text = $data['field_components'][0]['field_column1'][0]['field_text'][0]['value'];
    $col2_text = $data['field_components'][0]['field_column2'][0]['field_text'][0]['value'];
    $this->assertEquals($col1_text, "Lorem Ipsum Column 1", "Column 1 contains correct text string");
    $this->assertEquals($col2_text, "Lorem Ipsum Column 2", "Column 2 contains correct text string");
  }

  /**
   * Tests that the nested paragraphs load in a XML service.
   */
  public function testXml() {
    $account = $this->drupalCreateUser([]);
    $this->drupalLogin($account);

    $this->drupalGet("node/1", ['query' => ['_format' => 'xml']]);
    $this->assertSession()->statusCodeEquals(200);

    $this->assertSession()->responseContains("<value>Lorem Ipsum Column 1</value>", "Column 1 text string exists");
    $this->assertSession()->responseContains("<value>Lorem Ipsum Column 2</value>", "Column 2 text string exists");
  }

}
